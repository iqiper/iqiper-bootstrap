# How to setup your machine to develop `iqiper`
- [How to setup your machine to develop `iqiper`](#how-to-setup-your-machine-to-develop-iqiper)
		- [Why](#why)
		- [How](#how)
		- [Port map](#port-map)
		- [Link to documentation](#link-to-documentation)
	- [Create the `API` documentation on your machine](#create-the-api-documentation-on-your-machine)
	- [Setup the database](#setup-the-database)
	- [Setup `Keycloak`](#setup-keycloak)
### Why

To quickly spin-up a working copy of the project for local developpement only.

### How

Run the `./run.sh` script to spin up.

You should have the following environment variables set :

- `IQIPER_CLIENT_ROOT`: The path to client repository on your machine
- `IQIPER_INFRA_ROOT`: The path to client repository on your machine
- `IQIPER_DATA_ROOT`: The path to data folder on your machine. *It should NOT be in this repository folder*

You can create a `.env.local` script that will be automaticaly sourced by the `run.sh` script
An example script would be :
```sh
#!/bin/bash

export IQIPER_CLIENT_ROOT=$(realpath ../iqiper_client)
export IQIPER_INFRA_ROOT=$(realpath ../iqiper_infra)
export IQIPER_DATA_ROOT=$(realpath ../data)
```

More over, you should login in docker using a Gitlab token.

Go to your account, under `Settings -> Access tokens` and create a new `read_registry` token
then execute :

```sh
docker login registry.gitlab.com
```

Use your gitlab username and the token as the password.

You use `-d | --doc` to generate the project documentation.
You use `-c | --clean` to clean generate files.
You use `-f | --front` to run in foreground.
You use `-k | --kill` to kill every container and clean volumes.

To quit the launch `./run.sh`, use `Ctrl+c`

For example to launch `postgres` and `keycloak`, you could run:

```sh
./run.sh postgres keycloak
```

### Port map

| Service  |      Port      |  URL |
|----------|:--------------:|------:|
| Keycloak (TLS)	| 	8543		|	http://localhost:8543	|
| Keycloak 			| 	8580		|	http://localhost:8580	|
| Mailslurper (smtp)| 	2500    	|   --- |
| Mailslurper (web)	|	8084		|   http://localhost:8084 |
| openapi_mock		|	5000 		|   http://localhost:5000 |
| redoc				|	4444 		|   http://localhost:4444 |
| Mongo				|	27017 		|   --- |
| Redis				|	6979 		|   --- |
| PGAdmin			|	5050 		|   http://localhost:5050 |
| Nginx				|	4000 		|   http://localhost:4000 |
| Postgres			|	5432 		|   --- |

### Link to documentation

| Service  |      Port      |
|----------|:--------------:|
| Service mains		| 	http://localhost:4000/service_mains/service_mains/		|
| Client			| 	http://localhost:4000/client/					    	|


## Create the `API` documentation on your machine

To setup the database, you should clone the `iqiper-infra` repository.

You will then run in the cloned folder the following :

```sh
swagger-cli bundle -r api/iqiper.yml > api/iqiper.json # Will concat the *.yml into a json
redoc-cli bundle api/iqiper.json # Will create a `redoc-static.html` file in the current folder
```

## Setup the database

To setup the database, you should clone the `iqiper-database` repository.

You will then run in the cloned folder the following :

```sh
source .env.local # Will put the necessary environment variable into your shell session
diesel setup # Create the database and setup diesel runtime
diesel migration run # Apply the remaining migrations
```

## Setup `Keycloak`

To begin running the `oreille`, you'll need to configure and run `Keycloak`

1. Clone `iqiper-infra`
2. Launch in a separate terminal the `bootstrap` with at least the `keycloak` argument
3. In the `iqiper-infra` folder, go to `k8s/cluster_config`
4. Run 

```sh
terraform init # Initialize the plugins
terraform workspace new dev # Create the dev workspace
terraform workspace select dev # Select it
terraform apply --var keycloak_url=http://localhost:8580 --var keycloak_user=admin --var keycloak_password=123 --auto-approve # Deploy everything that's needed for the `oreille` project
```
