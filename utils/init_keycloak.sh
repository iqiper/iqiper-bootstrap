#!/bin/bash
docker run --rm -it \
    --name keycloak_import \
	--network=iqiper-bootstrap_default \
    -v $PWD/configs/keycloak:/tmp/keycloak-importer \
    -e KC_USER=admin \
	-e KC_PASS=123 \
	-e SERVER=http://0.0.0.0:8080/auth \
	--entrypoint=/bin/sh \
    quay.io/keycloak/keycloak \
	/tmp/keycloak-importer/import_master.sh
