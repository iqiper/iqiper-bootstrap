#!/bin/bash
export UID=${UID}
export GID=${GID}
docker run --rm \
	--user="${UID}:" \
    --name keycloak_exporter \
	--network=iqiper-bootstrap_default \
    -v /tmp:/tmp/keycloak-export \
    -v $PWD/ssl:/ssl:ro \
    -e DB_DATABASE=keycloak \
    -e DB_PASSWORD=password \
    -e DB_USER=keycloak \
	-e DB_SCHEMA=public \
    -e DB_VENDOR=POSTGRES \
    -e JDBC_PARAMS="ssl=true&sslmode=verify-ca&sslrootcert=/ssl/server.pem" \
    -e DB_ADDR=postgres \
    quay.io/keycloak/keycloak \
    -Dkeycloak.migration.action=export \
    -Dkeycloak.migration.provider=singleFile \
    -Dkeycloak.migration.file=/tmp/keycloak-export/keycloak-export.json \
    -Dkeycloak.migration.realmName=microcks
