#!/bin/bash
info() { printf "[INFO] (%s) %s\n" "$( date --iso-8601=ns )" "$*" >&2; }
error() { printf "[ERROR] (%s) %s\n" "$( date --iso-8601=ns )" "$*" >&2; }
trap 'error Interrupted; exit 2' INT TERM
if [ -f .env.local ]
then
	info "Loading env file"
	source .env.local
fi
GEN_DOC=0
CLEAN=0
KILL=0
FRONT="-d"
PARAMS=""
BUILD=""
DOCKER_COMPOSE_PARAM="-f docker-compose.yml"
while (( "$#" )); do
  case "$1" in
    -d|--doc)
    	GEN_DOC="1"
    	shift
    	;;
	-c|--clean)
		CLEAN="1"
		shift
		;;
	-b|--build)
		BUILD="--build"
		shift
		;;
	-f|--front)
		FRONT=""
		shift
		;;
	-k|--kill)
		KILL="1"
		shift
		;;
    -*|--*=)
    	error "Error: Unsupported flag $1" >&2
    	exit 1
    	;;
    *)
    	PARAMS="$PARAMS $1"
    	shift
    	;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"
IQIPER_SERVICE_MAINS_ROOT=$(realpath $IQIPER_SERVICE_MAINS_ROOT)
IQIPER_CLIENT_ROOT=$(realpath $IQIPER_CLIENT_ROOT)
IQIPER_INFRA_ROOT=$(realpath $IQIPER_INFRA_ROOT)
IQIPER_DATA_ROOT=$(realpath $IQIPER_DATA_ROOT)
swagger-cli bundle -r $IQIPER_INFRA_ROOT/api/iqiper.yml -o $IQIPER_INFRA_ROOT/api/iqiper.json
if [ -z $IQIPER_SERVICE_MAINS_ROOT ]
then
	error "Service Mains root not specified"
	exit 1
elif [ $GEN_DOC -ne 0 ]
then
	pushd $IQIPER_SERVICE_MAINS_ROOT
	cargo doc
	popd
fi

if [ -z $IQIPER_CLIENT_ROOT ]
then
	error "Client root not specified"
	exit 1
elif [ $GEN_DOC -ne 0 ]
then
	pushd $IQIPER_CLIENT_ROOT
	dartdoc --exclude "dart:async,dart:collection,dart:convert,dart:core,dart:developer,dart:ffi,dart:html,dart:io,dart:isolate,dart:js,dart:js_util,dart:math,dart:typed_data,dart:ui"
	popd
fi

if [ -z $IQIPER_INFRA_ROOT ]
then
	error "INFRA root not specified"
	exit 1
fi

if [[ $CLEAN != 0 ]]
then
	rm -rf $IQIPER_DATA_ROOT
	rm -rf ssl
	info "Cleaned !"
	exit 0
fi

if [[ $KILL != 0 ]]
then
	info "Stopping"
	trap '' INT TERM
	docker-compose $(echo $DOCKER_COMPOSE_PARAM) down -v
	exit 0
fi

if [ ! -d ssl ]
then
	info "SSL folder not found, recreating"
	./gen_ssl.sh >/dev/null
fi

mkdir -p $IQIPER_DATA_ROOT/postgres
mkdir -p $IQIPER_DATA_ROOT/mail
mkdir -p $IQIPER_DATA_ROOT/mongo
mkdir -p $IQIPER_DATA_ROOT/redis

export UID=${UID}
export GID=${GID}

if [ -f docker-compose.user.yml ]
then
	DOCKER_COMPOSE_PARAM="$DOCKER_COMPOSE_PARAM -f docker-compose.user.yml"
fi

info "Starting"

trap '' INT TERM
docker-compose $(echo $DOCKER_COMPOSE_PARAM) up $(echo $FRONT $BUILD $PARAMS)
watch docker-compose -f docker-compose.yml ps

docker-compose $(echo $DOCKER_COMPOSE_PARAM) down -v
