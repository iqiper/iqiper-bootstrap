#!/bin/sh
SSL_DIR="ssl"
SSL_PASS="changeit"
CAKEY="$SSL_DIR/CAKEY.pem"
CACERT="$SSL_DIR/ca.crt"
SERVER_REQ="$SSL_DIR/server.csr"
SERVER_KEY="$SSL_DIR/server.key"
SERVER_CRT="$SSL_DIR/server.crt"
SERVER_CRT_PEM="$SSL_DIR/server.pem"
SERVER_PKCS12="$SSL_DIR/server.p12"
mkdir -p ssl
openssl genrsa -des3 -passout pass:abcd -out $CAKEY 2048
openssl req -x509 -new -passin pass:abcd -nodes -key $CAKEY -sha256 -days 1024 -out $CACERT -subj /CN=localhost
openssl genrsa -out $SERVER_KEY 2048
openssl req -new -sha256 -key $SERVER_KEY -subj "/C=US/ST=CA/O=iqiper, Inc./CN=localhost" -out $SERVER_REQ
openssl x509 -passin pass:abcd -req -in $SERVER_REQ -CA $CACERT -CAkey $CAKEY -CAcreateserial -out $SERVER_CRT -days 1000 -sha256
openssl x509 -in $SERVER_CRT -out $SERVER_CRT_PEM -outform PEM
openssl pkcs12 -export -inkey $SERVER_KEY -in $SERVER_CRT_PEM -out $SERVER_PKCS12 -password pass:$SSL_PASS

chmod 600 $CAKEY
chmod 600 $SERVER_KEY
chmod 600 $SERVER_CRT
chmod 600 $SERVER_CRT_PEM
chmod 600 $SERVER_PKCS12
chown -R $(id -u):$(id -g) $SSL_DIR
