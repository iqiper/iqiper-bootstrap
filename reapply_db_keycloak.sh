#!/bin/bash
PGPASSWORD=postgres psql -U postgres -h localhost -d postgres -c "DROP DATABASE iqiper;"
cd ../iqiper-database
source .env.local
diesel setup
diesel migration run
cd ../iqiper-infra/k8s/cluster_config/
terraform apply --var keycloak_url=http://localhost:8580 --var keycloak_user=admin --var keycloak_password=123 --auto-approve
