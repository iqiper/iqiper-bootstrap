#!/bin/bash
/opt/jboss/keycloak/bin/kcadm.sh config credentials --server http://keycloak:8080/auth --realm master --user admin --password 123
/opt/jboss/keycloak/bin/kcadm.sh create partialImport -r master -s ifResourceExists=SKIP -o -f /tmp/keycloak-importer/master-realm.skipped.json
/opt/jboss/keycloak/bin/kcadm.sh create partialImport -r microcks -s ifResourceExists=SKIP -o -f /tmp/keycloak-importer/microcks-users-0.json
